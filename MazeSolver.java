/**
 * MazeSolver attempts to recursively traverse a Maze. The goal is to get from the
 * given starting position to the bottom right, follwing a path of 1's. Arbitrary 
 * constants are used to represent locations in the maze that have been TRIED
 * and that are part of the Solution PATH
 * 
 * @author Nightmare
 * @version 1.0
 */
public class MazeSolver {
    private Maze maze;

    /**
     * Constructor for the MazeSolver class.
     */
    public MazeSolver(Maze maze) {
        this.maze = maze;
    }

    /**
     * Attempts to recrusively traverse the maze. Inserts special
     * characters indicating locations that have been TRIED and that eventually
     * become part of the solution PATH.
     * 
     * @param row row index of current locations 
     * @param col column index of current locations 
     * return true if the maze has been solved
     */
    public boolean traverse(int row, int col) {
        boolean done = false;

        if (maze.validPosition(row, col)) {
            maze.tryPostion(row, col); //mark the cell as TRIED

            //Test to see if we are at the end of the maze
            if (row == maze.getRows() - 1 && col == maze.getColumns() - 1) {
                done = true; //the maze is solved
            } else {
                done = traverse(row + 1, col); // Test down
                if (!done) {
                    done = traverse(row, col + 1); //Test right
                }
                if (!done) {
                    done = traverse(row - 1, col); //Test up
                }
                if (!done) {
                    done = traverse(row, col - 1); //Test Left
                }
            }
            if (done) {
                maze.markPath(row, col); //This location is part of the final path
            }
        }
        return done;
    }
}