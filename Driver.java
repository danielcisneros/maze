/**
 * Driver tester to use recursion to determine if a maze can be
 * traversed
 * 
 * @author Daniel
 * @version 1.0
 */

import java.util.*;
import java.io.*;

public class Driver {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the name of the file containing the maze: ");
        String filename = scan.nextLine();

        Maze labyrinth = new Maze(filename);

        System.out.println(labyrinth);

        MazeSolver solver = new MazeSolver(labyrinth);

        if (solver.traverse(0,0)){
            System.out.println("The maze was successfully traversed");
        } else {
            System.out.println("There is no possible path.");
        }
        System.out.println(labyrinth);
        scan.close();
    }
}